#ifndef __INCLUDE_CLASS_LCD_FRAME_LCD_H__
#define __INCLUDE_CLASS_LCD_FRAME_LCD_H__

#include <wx/frame.h>
#include <wx/timer.h>
#include "LCD_Common.h"
#include "LCD_FrameBase.h"
#include "LCD_FrameLog.h"
#include "LCD_ScreenPanel.h"

#include "ctb/portscan.h"
#include "ctb/getopt.h"
#include "ctb/iobase.h"
#include "ctb/serport.h"
#include "ctb/timer.h"

#define wxID_LCD_FRAME								(10000)
#define wxID_LCD_TOOLBAR							(10001)
#define wxID_LCD_TOOLBAR_CLEARSCREEN				(10002)	// Clear screen.
#define wxID_LCD_TOOLBAR_COPYSCREENSHOTS			(10003)	// Copy screen shots.
#define wxID_LCD_TOOLBAR_QUICKSHOTS					(10004)	// Quick shot.
#define wxID_LCD_TOOLBAR_OPENSCREENSHOTSFOLDER		(10006)	// Open screen shots folder.
#define wxID_LCD_TOOLBAR_CONNECTDEVICE				(10007)	// Connected Device.
#define wxID_LCD_TOOLBAR_SHOWLOGWINDOW				(10008)	// Show log window.
#define wxID_LCD_TOOLBAR_SETTINGS					(10009)	// Settings.
#define wxID_LCD_TOOLBAR_ABOUT						(10010)	// About.
#define wxID_LCD_TOOLBAR_EXIT						(10011)	// Exit.
#define wxID_LCD_STATUSBAR							(10012)
#define wxID_LCD_PAINT_PANEL						(10013)
#define wxID_LCD_TIMER_RECIVE						(10014)

#define MAIN_FRAME_TITLE							_TRANS_TEXT("LCDSimulator")

///////////////////////////////////////////////////////////////////////////////
/// Class Device_Frame
///////////////////////////////////////////////////////////////////////////////
class LCD_DeviceFrame : public LCD_FrameBase
{
	private:
		// Controls pointer.
		wxToolBar*					m_pclsToolbar;
		wxStatusBar*				m_pclsStatusBar;
		wxTimer*					m_pclsDataReciveTimer;
		IOBase*						m_pclsSerialPort;
		LCD_FrameLog*				m_pclsLogFrame;
		LCD_ScreenPanel*			m_pclsPaintPanel;

		uint16_t					m_uiColumnIndex;
		uint16_t					m_uiPageIndex;

		// Event interface.
		void						wxEvent_OnClearScreen(wxCommandEvent &event)			{OnClearScreen();}
		void						wxEvent_OnQuickShot(wxCommandEvent &event)				{OnQuickShot();}
		void						wxEvent_OnCopyScreenshot(wxCommandEvent &event)			{OnCopyScreenshot();}
		void						wxEvent_OnOpenScreenshotsFolder(wxCommandEvent &event)	{OnOpenScreenShotsFolder();}
		void						wxEvent_OnDeviceConnect(wxCommandEvent &event)			{OnDeviceConnect();}
		void						wxEvent_OnShowLogWindow(wxCommandEvent &event)			{OnShowLogWindow();}
		void						wxEvent_OnClose(wxCloseEvent& event)					{OnClose(event);}
		void						wxEvent_OnExit(wxCommandEvent &event)					{Destroy();}
		void						wxEvent_OnTimerEvent(wxTimerEvent& event)				{OnTimerEvent(event);}
		void						wxEvent_OnKeyPress(wxKeyEvent& event)					{OnKeyPress(event);}

		void						wxEvent_OnAbout(wxCommandEvent &event)					{About();}
		void						wxEvent_OnSetting(wxCommandEvent &event)				{OnSetting();}
		void						wxEvent_OnControl(CLCDControlEvent& event)				{OnControl(event);}
		// Event process function
		virtual void				OnClose(wxCloseEvent& event)							{Destroy();}
		virtual void				OnKeyPress(wxKeyEvent& event);
		virtual void				OnClearScreen(void);
		virtual void				OnQuickShot(void);
		virtual void				OnCopyScreenshot(void);
		virtual	void				OnOpenScreenShotsFolder(void);
		virtual void				OnDeviceConnect(void);
		virtual void				OnShowLogWindow(void);
		virtual void				OnTimerEvent(wxTimerEvent& event);
		virtual void				OnChildWindowClose(wxWindowID iFramrID = wxID_ANY);
		virtual void				OnControl(CLCDControlEvent& event);
		// Private member function.
		virtual wxToolBar*			CreateLCDToolBar(void);
		virtual void				UpdateStatusBarText(const wxString& strText);
		virtual void				OnSetting(void);
		virtual void				About(void);
		virtual void				AppendLog(wxString strLog);
		bool						OpenSerialPort(void);
		void						CloseSerialPort(void);
	public:
									LCD_DeviceFrame(	LCD_FrameBase* parent,
														wxWindowID id				= wxID_ANY,
														const wxString& title		= MAIN_FRAME_TITLE,
														const wxPoint& pos			= wxDefaultPosition,
														const wxSize& size			= wxDefaultSize,
														long style					= wxCAPTION|wxCLOSE_BOX|wxTAB_TRAVERSAL);
									~LCD_DeviceFrame(void);
		void						OnResopnse(uint8_t uiCommand, uint16_t uiData);

		DECLARE_EVENT_TABLE();
};


#endif // __INCLUDE_CLASS_LCD_FRAME_LCD_H__
