#ifndef __INCLUDE_CLASS_LCD_FRAME_BASE_H__
#define __INCLUDE_CLASS_LCD_FRAME_BASE_H__

#include <wx/toolbar.h>
#include <wx/toolbar.h>
#include <wx/frame.h>
#include "LCD_Common.h"

class LCD_FrameBase : public wxFrame
{
	public:
									LCD_FrameBase(		wxWindow* parent,
														wxWindowID id = wxID_ANY,
														const wxString& title = wxEmptyString,
														const wxPoint& pos = wxDefaultPosition,
														const wxSize& size = wxSize( 500,300 ),
														long style = wxCAPTION|wxCLOSE_BOX|wxTAB_TRAVERSAL);
									~LCD_FrameBase();
		virtual void				OnChildWindowClose(wxWindowID iFramrID = wxID_ANY) = 0;
		virtual void				OnResopnse(uint8_t uiCommand, uint16_t uiData);


	private:


};

#endif // __INCLUDE_CLASS_LCD_FRAME_BASE_H__
