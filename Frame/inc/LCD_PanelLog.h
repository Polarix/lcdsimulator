#ifndef __INCLUDE_CLASS_PANELLOG_H__
#define __INCLUDE_CLASS_PANELLOG_H__

#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include "LCD_Common.h"

///////////////////////////////////////////////////////////////////////////////
/// Class Log_Panel
///////////////////////////////////////////////////////////////////////////////
class LCD_PanelLog : public wxPanel
{
	private:
		wxBoxSizer*			m_pclsTextCtrlSizer;
		wxTextCtrl*			m_pclsTextCtrl;

	protected:

	public:
							LCD_PanelLog(	wxWindow* parent,
											wxWindowID id = wxID_ANY,
											const wxPoint& pos = wxDefaultPosition,
											const wxSize& size = wxDefaultSize,
											long style = wxTAB_TRAVERSAL);
							~LCD_PanelLog();
		void				AppendText(const wxString& strText);
};

#endif // __INCLUDE_CLASS_PANELLOG_H__
