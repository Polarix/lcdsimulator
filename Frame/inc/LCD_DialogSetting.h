#ifndef __INCLUDE_CLASS_LCD_DIALOG_SETTINGS_H__
#define __INCLUDE_CLASS_LCD_DIALOG_SETTINGS_H__

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////
#include <vector>

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/gdicmn.h>
#include <wx/notebook.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/spinctrl.h>
#include <wx/string.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/colordlg.h>
#include <wx/filedlg.h>
#include "LCD_ScreenPanel.h"
#include "LCD_Common.h"

#include "ctb/portscan.h"

///////////////////////////////////////////////////////////////////////////
#define wxID_SETTING_DIALOG					(30000)
#define wxID_COMBOBOX_PORT					(30001)
#define wxID_COMBOBOX_BAUDRATE				(30002)
#define wxID_SETTING_SPINCTRL_COLUMNS		(30003)
#define wxID_SETTING_SPINCTRL_PAGES			(30004)
#define wxID_SETTING_SPINCTRL_PIXELSIZE		(30005)
#define wxID_SETTING_BUTTON_BACKGROUNDCOLOR	(30006)
#define wxID_SETTING_BUTTON_PIXELCOLOR		(30007)
#define wxID_SETTING_BUTTON_EDGECOLOR		(30008)
#define wxID_SETTING_BUTTON_GRIDCOLOR		(30009)
#define wxID_SETTING_CHECK_GRIDENABLE		(30010)
#define wxID_SETTING_BUTTON_IMPORTFILE		(30011)

#define SETTING_DIALOG_TITLE						_TRANS_TEXT("LCDSimulator Settings")
///////////////////////////////////////////////////////////////////////////////
/// Class LCD_SettingFrame
///////////////////////////////////////////////////////////////////////////////

using std::vector;
class LCD_DialogSettings : public wxDialog
{
	private:
		wxBoxSizer*				m_pclsSizer_DialogMain;
		wxStaticBoxSizer*		m_pclsStaticBoxSizer_ComPort;
		wxBoxSizer*				m_pclsSizer_ComPort;
		wxStaticText*			m_pclsStaticText_ComPort;
		wxComboBox*				m_pclsComboBox_ComPort;
		wxStaticText*			m_pclsStaticText_Baudrate;
		wxComboBox*				m_pclsComboBox_Baudrate;
		wxStaticBoxSizer*		m_pclsStaticBoxSizer_Appearance;
		wxStaticText*			m_pclsStaticText_Columns;
		wxSpinCtrl*				m_pclsSpinCtrl_Columns;
		wxStaticText*			m_pclsStaticText_Pages;
		wxSpinCtrl*				m_pclsSpinCtrl_Pages;
		wxStaticText*			m_pclsStaticText_PixelSize;
		wxSpinCtrl*				m_pclsSpinCtrl_PixelSize;
		wxStaticBoxSizer*		m_pclsStaticBoxSizer_Colors;
		wxBoxSizer*				m_pclsSizer_ColorSettings;
		wxButton*				m_pclsButtonScreenBackColor;
		wxButton*				m_pclsButtonScreenPixelColor;
		wxButton*				m_pclsButtonScreenEdgeColor;
		wxCheckBox*				m_pclsCheckBoxEnableGrid;
		wxButton*				m_pclsButtonScreenGridColor;
		wxBoxSizer*				m_pclsSizerPreview;
		LCD_ScreenPanel*		m_pclsPanelPreview;
		wxBoxSizer*				m_pclsSizer_DialogButtons;
		//wxButton*				m_pclsButtonImportConfigFile;
		wxStdDialogButtonSizer*	m_pclsStdDialodSizer_DialogButtonsSizer;
		wxButton*				m_pclsStdDialodSizer_DialogButtonsSizerOK;
		wxButton*				m_pclsStdDialodSizer_DialogButtonsSizerCancel;
		vector<PortInfo>		m_clsRtnDevicesInfoList;

		bool					m_bPreviewRefreshed;
		//PixelPanel::Parameter	m_stPreviewParameter;
		SimulatorParameter		m_stSettingParameter;

		void					wxEvent_OnUpdateUI(wxUpdateUIEvent& event)			{OnUpdateUI(event);}
		void					wxEvent_OnSpinCtrlValueUpdate(wxSpinEvent& event)	{UpdatePreviewPanel();}
		void					wxEvent_OnColorSetChanged(wxCommandEvent& event)	{UpdatePreviewColor(event);}
		void					wxEvent_OnLoadConfigFile(wxCommandEvent& event)		{LoadConfigFormFile();}
		void					wxEvent_OnGridEnable(wxCommandEvent& event)			{OnGridEnable();}
		void					wxEvent_OnPortSettingChanged(wxCommandEvent& event)	{OnPortSet();}
		virtual void			OnUpdateUI(wxUpdateUIEvent& event);

		void					UpdatePreviewPanel(void);
		void					UpdatePreviewColor(wxCommandEvent& event);
		void					LoadConfigFormFile(void);
		void					OnGridEnable(void);
		void					OnPortSet(void);

	protected:

	public:
								LCD_DialogSettings( wxWindow* parent,
													wxWindowID id = wxID_SETTING_DIALOG,
													const wxString& title = SETTING_DIALOG_TITLE,
													const wxPoint& pos = wxDefaultPosition,
													const wxSize& size = wxSize( 456,430 ),
													long style = wxDEFAULT_DIALOG_STYLE );
								~LCD_DialogSettings()								{}
		virtual int				ShowDialog(SimulatorParameter* pstParameterData);

		DECLARE_EVENT_TABLE();
};

#endif // LCD_FRAMESETTING_H_INCLUDED
