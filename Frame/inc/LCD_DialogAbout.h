///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __INCLUDE_CLASS_LCD_DIALOG_ABOUT_H__
#define __INCLUDE_CLASS_LCD_DIALOG_ABOUT_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include <wx/notebook.h>
#include <wx/dialog.h>

#include <LCD_Common.h>


///////////////////////////////////////////////////////////////////////////////
/// Class OEMDialog
///////////////////////////////////////////////////////////////////////////////
class OEMDialog : public wxDialog
{
	private:
		wxStaticBitmap*		m_pclsBitmapOEM;
		wxNotebook*			m_pclsNotebookInfo;
		wxPanel*			m_pclsPanelIntroduction;
		wxTextCtrl*			m_pcslTextCtrlIntroduction;
		wxPanel*			m_pclsPanelDeveloper;
		wxTextCtrl*			m_pcslTextCtrlDevelopers;
	protected:
		void				Initialize(void);

	public:

		OEMDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _TRANS_TEXT("About"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );
		~OEMDialog();
};

#endif //__INCLUDE_CLASS_LCD_DIALOG_ABOUT_H__
