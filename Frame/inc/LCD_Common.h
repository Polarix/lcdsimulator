#ifndef __INCLUDE_LCD_COMMON_H__
#define __INCLUDE_LCD_COMMON_H__

#include <stdint.h>
#include <wx/wx.h>

#define wxID_LOG_FRAME							    	(20001)
#define wxID_LOG_TEXT_PANEL						    	(20002)

#define LCD_COLOR_OBJ_PIX						    	(0xFF168363)
#define LCD_COLOR_OBJ_BKG						    	(0xFF00F0D7)
#define LCD_COLOR_OBJ_EDGE						    	(0xFF383834)
#define LCD_COLOR_OBJ_GRID						    	(0xFF383834)

#define LCD_DEFAULT_CONFIG_FILE					    	("Config.xml")

#define PARAM_DEFAULT_COLUMNS						    (192)
#define PARAM_DEFAULT_PAGES					    		(8)
#define PARAM_DEFAULT_PIXEL					    		(3)
#define PARAM_DEFAULT_EDGE					    		(5)
#define PARAM_DEFAULT_GRID					    		(false)

#define SHORTCUT_KEY_CLEARSCREEN						('D')
#define SHORTCUT_KEY_COPYSCREENSHOTS					('C')
#define SHORTCUT_KEY_QUICKSHOTS							('P')
#define SHORTCUT_KEY_SAVESCREENSHOTS					('S')
#define SHORTCUT_KEY_OPENSCREENSHOTSFOLDER				('O')

#define SERIAL_EVENT_DATA_READ_PEREVENT					(32)
#define SERIAL_EVENT_DATA_INDEX_COMMAND					(0)
#define SERIAL_EVENT_DATA_INDEX_VALUE_L					(1)
#define SERIAL_EVENT_DATA_INDEX_VALUE_H					(2)
#define SERIAL_EVENT_DATA_SIZE							(3)

#define _TRANS_TEXT(STR)								_(STR)

typedef enum
{
	SHORTCUT_KEY_INDEX_CLEARSCREEN = 0,
	SHORTCUT_KEY_INDEX_COPYSCREENSHOTS,
	SHORTCUT_KEY_INDEX_QUICKSHOTS,
	SHORTCUT_KEY_INDEX_SAVESCREENSHOTS,
	SHORTCUT_KEY_INDEX_OPENSCREENSHOTSFOLDER,

	SHORTCUT_KEY_INDEX_MAX
}SHORTCUT_KEY_INDEX;

typedef struct
{
	uint16_t			Index;
	char				Text[16];
	uint16_t			KeyCode;
}SHORTCUT_KEY;

typedef struct
{
	char					PortName[32];
	uint32_t				Baudrate;
}CommPort;

typedef union
{
	uint8_t					Channel[4];
	uint32_t				RGBA;
}LCDRGBAColor;


typedef struct
{
    size_t					Columns;
    size_t					Pages;
    size_t					PixelSize;
    size_t					EdgeWidth;
}LCDPanelSize;

typedef struct
{
	LCDRGBAColor			Edge;
	LCDRGBAColor			Panel;
	LCDRGBAColor			Pixel;
	LCDRGBAColor			Grid;
}LCDPanelColor;

typedef struct
{
	LCDPanelSize			Size;
	LCDPanelColor			Color;
	bool					EnableGrid;
}LCDPanelParameter;

typedef struct
{
	CommPort				Port;
	LCDPanelParameter		Panel;
}SimulatorParameter;

typedef struct
{
	size_t					HorizontalPixelNumber;
    size_t					VerticalPixelNumber;
    size_t					PixelSize;
    size_t					EdgeWidth;

	LCDRGBAColor			EdgeColor;
	LCDRGBAColor			PanelColor;
	LCDRGBAColor			PixelColor;
	LCDRGBAColor			GridColor;

	bool					EnableGrid;
}PixelPanelParameter;

typedef enum
{
	CMD_ID_NONE = 0,
	CMD_SET_POS_COLUMN,
	CMD_SET_POS_PAGE,
	CMD_WRITE_DATA,
	CMD_READ_DATA,
	CMD_REFRESH,
	CMD_CLEAN,
	CMD_ID_MAX,
}SerialCommand;

extern SimulatorParameter	g_stParameters;
extern SHORTCUT_KEY			g_arrstShortcutKeyTable[];
extern const char*			g_arrszCommandName[];

void						SetDefaultParameterData(SimulatorParameter* pstParameter);
void						ReadConfigFile(wxString& strConfigurationFilePath, SimulatorParameter* pstParameter);
void						SaveParameter(SimulatorParameter* pstParameter);
void						ReadParameter(wxString& strFilePath, SimulatorParameter* pstParameter);

#define evtID_LCD_PANEL_CTRL_EVENT		(wxID_HIGHEST+100)

class CLCDControlEvent : public wxEvent
{
    public:
							CLCDControlEvent(wxEventType cmdType = wxEVT_NULL,int id = wxID_ANY);
							CLCDControlEvent(const CLCDControlEvent& event);
        virtual wxEvent*	Clone() const { return new CLCDControlEvent(*this);}

    public:
    	void				SetCommandID(uint8_t uiCommandID);
    	uint8_t				GetCommandID(void) const;
    	void				SetData(uint16_t uiData);
    	void				SetData(uint8_t uiDataHighByte, uint8_t uiDataLowByte);
		uint16_t			GetData(void) const;
    protected:

    private:
    	uint8_t				m_uiCommandID;
    	uint16_t			m_uiData;
        DECLARE_DYNAMIC_CLASS(CLCDControlEvent)
};

typedef void (wxEvtHandler::*wxCReciveDataEventFunction)(CLCDControlEvent&);

BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_LOCAL_EVENT_TYPE(wxEVT_ON_DATA_RECIVED, evtID_ON_DATA_RECIVED)
END_DECLARE_EVENT_TYPES()

#define EVT_DATA_RECIVED(fn) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_ON_DATA_RECIVED, -1, -1, (wxObjectEventFunction)(wxEventFunction)(wxCReciveDataEventFunction)&fn, (wxObject*)NULL),

#endif // __INCLUDE_LCD_COMMON_H__
