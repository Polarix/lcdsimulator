#ifndef __INCLUDE_CLASS_FRAME_LOG_H__
#define __INCLUDE_CLASS_FRAME_LOG_H__

#include <wx/frame.h>
#include <wx/panel.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>

#include "LCD_Common.h"
#include "LCD_FrameBase.h"
#include "LCD_PanelLog.h"

#define LOG_FRAME_TITLE						_TRANS_TEXT("LCDSimulator Log")

///////////////////////////////////////////////////////////////////////////////
/// Class LCD_LogFrame
///////////////////////////////////////////////////////////////////////////////
class LCD_FrameLog : public LCD_FrameBase
{
	private:
		LCD_PanelLog*			m_pclsLogTextPanel;
		LCD_FrameBase*			m_pclsParentFrame;

		void					wxEvent_OnControl(CLCDControlEvent& event)			{OnControl(event);}
		void					wxEvent_OnClose(wxCloseEvent& event)					{OnClose(event);}
	protected:
		virtual void			OnControl(CLCDControlEvent& event);
		virtual void			OnClose(wxCloseEvent& event)							{Destroy();}
	public:

								LCD_FrameLog(	LCD_FrameBase* parent,
												wxWindowID id				= wxID_ANY,
												const wxString& title		= LOG_FRAME_TITLE,
												const wxPoint& pos			= wxDefaultPosition,
												const wxSize& size			= wxDefaultSize,
												long style 					= wxCAPTION|wxCLOSE_BOX|wxMAXIMIZE_BOX|wxMINIMIZE_BOX|wxRESIZE_BORDER|wxFRAME_FLOAT_ON_PARENT|wxFRAME_NO_TASKBAR);
								~LCD_FrameLog(void);
		void					AppendText(const wxString& strText);
		virtual void			OnChildWindowClose(wxWindowID iFramrID = wxID_ANY){};

		DECLARE_EVENT_TABLE();
};

#endif // __INCLUDE_CLASS_FRAME_LOG_H__
