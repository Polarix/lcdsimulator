
#include "LCD_PanelLog.h"

LCD_PanelLog::LCD_PanelLog(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : wxPanel( parent, id, pos, size, style )
{
	m_pclsTextCtrlSizer = new wxBoxSizer( wxVERTICAL );

	m_pclsTextCtrl = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(1920, 1080), wxTE_MULTILINE );
	m_pclsTextCtrl->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pclsTextCtrl->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	m_pclsTextCtrlSizer->Add(m_pclsTextCtrl, 0, wxALL, 5 );


	SetSizer(m_pclsTextCtrlSizer);
	Layout();
}

void LCD_PanelLog::AppendText(const wxString& strText)
{
	m_pclsTextCtrl->AppendText(strText);
}

LCD_PanelLog::~LCD_PanelLog(void)
{

}
