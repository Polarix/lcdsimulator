#include <wx/window.h>

#include "LCD_Common.h"
#include "LCD_FrameBase.h"
#include "LCD_FrameLog.h"

BEGIN_EVENT_TABLE(LCD_FrameLog,LCD_FrameBase)
    EVT_CLOSE			(LCD_FrameLog::wxEvent_OnClose)
    EVT_DATA_RECIVED	(LCD_FrameLog::wxEvent_OnControl)
END_EVENT_TABLE()
LCD_FrameLog::LCD_FrameLog(LCD_FrameBase* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style):
LCD_FrameBase(parent, id, title, pos, size, style)
{
	SetSizeHints( wxDefaultSize, wxDefaultSize );
	m_pclsLogTextPanel = new LCD_PanelLog(this, wxID_LOG_TEXT_PANEL);
	m_pclsParentFrame = parent;
	SetIcon(wxIcon(_T("ID_ICON_LOG_FRAME"), wxBITMAP_TYPE_ICO_RESOURCE));
}

void LCD_FrameLog::AppendText(const wxString& strText)
{
	m_pclsLogTextPanel->AppendText(strText);
}

LCD_FrameLog::~LCD_FrameLog()
{
	m_pclsParentFrame->OnChildWindowClose(GetId());
}

void LCD_FrameLog::OnControl(CLCDControlEvent& event)
{
	SerialCommand		eCommandID;
	eCommandID = (SerialCommand)event.GetCommandID();
	if(eCommandID >= CMD_ID_MAX)
	{
		eCommandID = CMD_ID_NONE;
	}
	AppendText(wxString::Format(_TRANS_TEXT("Command ID: %u; Data: %u; %s(0x%04X).\n"), eCommandID, event.GetData(), g_arrszCommandName[eCommandID], event.GetData()));
}

