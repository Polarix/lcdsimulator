#include <wx/xml/xml.h>
#include <LCD_Common.h>

#define			CFG_XML_NODE_NAME_ROOT					("LcdSimulator")

#define 		CFG_XML_NODE_NAME_COMM					("Port")
#define			CFG_XML_ATTR_NAME_PORT					("Name")
#define			CFG_XML_ATTR_NAME_BAUDRATE				("Baudrate")

#define			CFG_XML_NODE_NAME_APPER					("Panel")
#define			CFG_XML_ATTR_NAME_COLUMNS				("Coloumns")
#define			CFG_XML_ATTR_NAME_PAGES					("Pages")
#define			CFG_XML_ATTR_NAME_PIXSIZE				("PixelSize")
#define			CFG_XML_ATTR_NAME_EDGEWIDTH				("EdgeWidth")

#define			CFG_XML_NODE_NAME_COLOR					("Color")
#define			CFG_XML_NODE_NAME_PANEL					("Panel")
#define			CFG_XML_NODE_NAME_PIX					("Pixel")
#define			CFG_XML_NODE_NAME_EDGE					("Edge")
#define			CFG_XML_NODE_NAME_GRID					("Grid")
#define			CFG_XML_ATTR_NAME_ENABLE				("Enable")
#define			CFG_XML_ATTR_NAME_COLOR_VALUE			("RGBA")

SimulatorParameter			g_stParameters;

SHORTCUT_KEY				g_arrstShortcutKeyTable[] = {
{SHORTCUT_KEY_INDEX_CLEARSCREEN,			"D", 'D'},
{SHORTCUT_KEY_INDEX_COPYSCREENSHOTS,		"C", 'C'},
{SHORTCUT_KEY_INDEX_QUICKSHOTS,				"P", 'P'},
{SHORTCUT_KEY_INDEX_SAVESCREENSHOTS,		"S", 'S'},
{SHORTCUT_KEY_INDEX_OPENSCREENSHOTSFOLDER,	"O", 'O'},
};

const char*					g_arrszCommandName[] = {
	_TRANS_TEXT("Invalid"),
	_TRANS_TEXT("Set Column"),
	_TRANS_TEXT("Set Page"),
	_TRANS_TEXT("Write Data"),
	_TRANS_TEXT("Read Data"),
	_TRANS_TEXT("Refresh Screen"),
	_TRANS_TEXT("Clean Screen")
};

void ReadConfigFile(wxString& strConfigurationFilePath, SimulatorParameter* pstParameter)
{
	if(false == wxFileExists(strConfigurationFilePath))
	{
		// Configuration file not existed.
		SetDefaultParameterData(pstParameter);
		SaveParameter(pstParameter);
	}
	else
	{
		ReadParameter(strConfigurationFilePath, pstParameter);
	}
}

void SaveParameter(SimulatorParameter* pstParameter)
{
	if(NULL != pstParameter)
	{
		wxXmlDocument clsXmlDoc;
		// Create root node.
		wxXmlNode* pclsRootNode = new wxXmlNode(NULL, wxXML_ELEMENT_NODE, CFG_XML_NODE_NAME_ROOT);
		clsXmlDoc.SetRoot(pclsRootNode);

		// Save comm port.
		wxXmlNode* pclsCommNode = new wxXmlNode(NULL, wxXML_ELEMENT_NODE, CFG_XML_NODE_NAME_COMM);
		pclsCommNode->AddAttribute(wxString(CFG_XML_ATTR_NAME_PORT), wxString(pstParameter->Port.PortName));
		pclsCommNode->AddAttribute(wxString(CFG_XML_ATTR_NAME_BAUDRATE), wxString::Format(wxT("%u"), pstParameter->Port.Baudrate));
		pclsRootNode->AddChild(pclsCommNode);

		// Size
		wxXmlNode* pclsAppearanceNode = new wxXmlNode(NULL, wxXML_ELEMENT_NODE, CFG_XML_NODE_NAME_APPER);
		pclsAppearanceNode->AddAttribute(wxString(CFG_XML_ATTR_NAME_COLUMNS), wxString::Format(wxT("%u"), pstParameter->Panel.Size.Columns));
		pclsAppearanceNode->AddAttribute(wxString(CFG_XML_ATTR_NAME_PAGES), wxString::Format(wxT("%u"), pstParameter->Panel.Size.Pages));
		pclsAppearanceNode->AddAttribute(wxString(CFG_XML_ATTR_NAME_PIXSIZE), wxString::Format(wxT("%u"), pstParameter->Panel.Size.PixelSize));
		pclsAppearanceNode->AddAttribute(wxString(CFG_XML_ATTR_NAME_EDGEWIDTH), wxString::Format(wxT("%u"), pstParameter->Panel.Size.EdgeWidth));
		pclsRootNode->AddChild(pclsAppearanceNode);

		// ScreenColor
		wxXmlNode* pclsColorNode = new wxXmlNode(NULL, wxXML_ELEMENT_NODE, CFG_XML_NODE_NAME_COLOR);

		wxXmlNode* pclsColorNode_Panel = new wxXmlNode(NULL, wxXML_ELEMENT_NODE, CFG_XML_NODE_NAME_PANEL);
		pclsColorNode_Panel->AddAttribute(wxString(CFG_XML_ATTR_NAME_COLOR_VALUE), wxString::Format(wxT("%08X"), pstParameter->Panel.Color.Panel.RGBA));
		pclsColorNode->AddChild(pclsColorNode_Panel);

		wxXmlNode* pclsColorNode_Pixel = new wxXmlNode(NULL, wxXML_ELEMENT_NODE, CFG_XML_NODE_NAME_PIX);
		pclsColorNode_Pixel->AddAttribute(wxString(CFG_XML_ATTR_NAME_COLOR_VALUE), wxString::Format(wxT("%08X"), pstParameter->Panel.Color.Pixel.RGBA));
		pclsColorNode->AddChild(pclsColorNode_Pixel);

		wxXmlNode* pclsColorNode_Edge = new wxXmlNode(NULL, wxXML_ELEMENT_NODE, CFG_XML_NODE_NAME_EDGE);
		pclsColorNode_Edge->AddAttribute(wxString(CFG_XML_ATTR_NAME_COLOR_VALUE), wxString::Format(wxT("%08X"), pstParameter->Panel.Color.Edge.RGBA));
		pclsColorNode->AddChild(pclsColorNode_Edge);

		wxXmlNode* pclsColorNode_Grid = new wxXmlNode(NULL, wxXML_ELEMENT_NODE, CFG_XML_NODE_NAME_GRID);
		pclsColorNode_Grid->AddAttribute(wxString(CFG_XML_ATTR_NAME_COLOR_VALUE), wxString::Format(wxT("%08X"), pstParameter->Panel.Color.Grid.RGBA));
		pclsColorNode_Grid->AddAttribute(wxString(CFG_XML_ATTR_NAME_ENABLE), wxString::Format(wxT("%s"), (pstParameter->Panel.EnableGrid==true)?wxString("true"):wxString("false")));
		pclsColorNode->AddChild(pclsColorNode_Grid);

		pclsRootNode->AddChild(pclsColorNode);

		// Save configuration file
		clsXmlDoc.Save(wxString(LCD_DEFAULT_CONFIG_FILE));
	}
}

void ReadParameter(wxString& strFilePath, SimulatorParameter* pstParameter)
{
	if(NULL != pstParameter)
	{
		// Load default parameter
		SetDefaultParameterData(pstParameter);
		// Create root node.
		wxXmlDocument clsXmlDoc;
		if(true == clsXmlDoc.Load(strFilePath, "UTF-8", wxXMLDOC_KEEP_WHITESPACE_NODES))
		{
			wxXmlNode*	pclsRootNode = clsXmlDoc.GetRoot();
			if(NULL == pclsRootNode)
			{
				// Get root node failed.
			}
			else
			{
				wxXmlNode*	pclsParamererNode = pclsRootNode->GetChildren();
				long		lReadValue = 0;
				while(NULL != pclsParamererNode)
				{
					if(pclsParamererNode->GetName() == CFG_XML_NODE_NAME_COMM)
					{
						// Port name
						strcpy(pstParameter->Port.PortName, pclsParamererNode->GetAttribute(CFG_XML_ATTR_NAME_PORT, wxString("COM1").c_str().AsChar()));
						// Baudrate
						pclsParamererNode->GetAttribute(CFG_XML_ATTR_NAME_BAUDRATE, wxString("115200")).ToLong(&lReadValue, 10);
						pstParameter->Port.Baudrate = (uint32_t)lReadValue;
					}
					else if(pclsParamererNode->GetName() == CFG_XML_NODE_NAME_APPER)
					{
						pclsParamererNode->GetAttribute(CFG_XML_ATTR_NAME_COLUMNS, wxString("128")).ToLong(&lReadValue, 10);
						pstParameter->Panel.Size.Columns = (uint32_t)lReadValue;
						pclsParamererNode->GetAttribute(CFG_XML_ATTR_NAME_PAGES, wxString("64")).ToLong(&lReadValue, 10);
						pstParameter->Panel.Size.Pages = (uint32_t)lReadValue;
						pclsParamererNode->GetAttribute(CFG_XML_ATTR_NAME_PIXSIZE, wxString("2")).ToLong(&lReadValue, 10);
						pstParameter->Panel.Size.PixelSize = (uint32_t)lReadValue;
						pclsParamererNode->GetAttribute(CFG_XML_ATTR_NAME_EDGEWIDTH, wxString("5")).ToLong(&lReadValue, 10);
						pstParameter->Panel.Size.EdgeWidth = (uint32_t)lReadValue;
					}
					else if(pclsParamererNode->GetName() == CFG_XML_NODE_NAME_COLOR)
					{
						wxXmlNode*	pclsColorNode = pclsParamererNode->GetChildren();
						while(NULL != pclsColorNode)
						{
							unsigned long		ulColorHEX;

							pclsColorNode->GetAttribute(wxString(CFG_XML_ATTR_NAME_COLOR_VALUE), wxString("0x000A0A0A")).ToULong(&ulColorHEX, 16);

							if(pclsColorNode->GetName() == CFG_XML_NODE_NAME_PANEL)
							{
								pstParameter->Panel.Color.Panel.RGBA = (uint32_t)ulColorHEX;
							}
							else if(pclsColorNode->GetName() == CFG_XML_NODE_NAME_PIX)
							{
								pstParameter->Panel.Color.Pixel.RGBA = (uint32_t)ulColorHEX;
							}
							else if(pclsColorNode->GetName() == CFG_XML_NODE_NAME_EDGE)
							{
								pstParameter->Panel.Color.Edge.RGBA = (uint32_t)ulColorHEX;
							}
							else if(pclsColorNode->GetName() == CFG_XML_NODE_NAME_GRID)
							{
								pstParameter->Panel.Color.Grid.RGBA = (uint32_t)ulColorHEX;
								if(wxString("true") == pclsColorNode->GetAttribute(wxString(CFG_XML_ATTR_NAME_ENABLE), wxString("false")))
								{
									pstParameter->Panel.EnableGrid = true;
								}
								else
								{
									pstParameter->Panel.EnableGrid = false;
								}
							}
							pclsColorNode = pclsColorNode->GetNext();
						}
					}
					pclsParamererNode = pclsParamererNode->GetNext();
				}
			}
		}
	}
}

void SetDefaultParameterData(SimulatorParameter* pstParameter)
{
	if(NULL != pstParameter)
	{
		// Comm port
		strcpy(pstParameter->Port.PortName, "COM1");
		pstParameter->Port.Baudrate = 115200;

		// Appearance
		pstParameter->Panel.Size.Columns = PARAM_DEFAULT_COLUMNS;
		pstParameter->Panel.Size.Pages = PARAM_DEFAULT_PAGES;
		pstParameter->Panel.Size.PixelSize = PARAM_DEFAULT_PIXEL;
		pstParameter->Panel.Size.EdgeWidth = PARAM_DEFAULT_EDGE;

		// ScreenColor
		pstParameter->Panel.Color.Panel.RGBA = LCD_COLOR_OBJ_BKG;
		pstParameter->Panel.Color.Pixel.RGBA = LCD_COLOR_OBJ_PIX;
		pstParameter->Panel.Color.Edge.RGBA = LCD_COLOR_OBJ_EDGE;
		pstParameter->Panel.Color.Grid.RGBA = LCD_COLOR_OBJ_GRID;

		pstParameter->Panel.EnableGrid = false;
	}
}

IMPLEMENT_DYNAMIC_CLASS(CLCDControlEvent, wxEvent)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_ON_DATA_RECIVED)

CLCDControlEvent::CLCDControlEvent(wxEventType iCommandType, int iID):
wxEvent(iCommandType,iID)
{
	SetCommandID(SHORTCUT_KEY_INDEX_MAX);
	SetData(0x0000);
}

CLCDControlEvent::CLCDControlEvent(const CLCDControlEvent& event):
wxEvent(event.GetEventType(), event.GetId())
{
	SetCommandID(event.GetCommandID());
	SetData(event.GetData());
}

void CLCDControlEvent::SetCommandID(uint8_t uiCommandID)
{
	m_uiCommandID = uiCommandID;
}

uint8_t CLCDControlEvent::GetCommandID(void) const
{
	return m_uiCommandID;
}

void CLCDControlEvent::SetData(uint16_t uiData)
{
	m_uiData = uiData;
}

void CLCDControlEvent::SetData(uint8_t uiDataHighByte, uint8_t uiDataLowByte)
{
	m_uiData = (((uint16_t)uiDataHighByte << 8) | ((uint16_t)uiDataLowByte & 0x00FF));
}

uint16_t CLCDControlEvent::GetData(void) const
{
	return m_uiData;
}
