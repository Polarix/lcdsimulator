//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/settings.h>
#include "LCD_Common.h"
#include "LCD_FrameBase.h"
#include "LCD_FrameLCD.h"
#include "LCD_FrameLog.h"
#include "wxPixelPanel.h"
#include "LCD_DialogSetting.h"
#include "LCD_DialogAbout.h"

#define SCREENSHOTS_FOLDER_T				"ScreenShots"
#define SCREENSHOTS_FILE_NAME_T				"LCD_%04u%02u%02u_%02u%02u-%u.jpg"
#define SCREENSHOTS_FILE_FULLNAME_T			wxString::Format("%s\\%s", SCREENSHOTS_FOLDER_T, SCREENSHOTS_FILE_NAME_T)
#define N_YEAR								wxDateTime::Now().GetYear()
#define N_MONTH								wxDateTime::Now().GetMonth()
#define N_DAY								wxDateTime::Now().GetDay()
#define N_HOUR								wxDateTime::Now().GetHour()
#define N_MINUTE							wxDateTime::Now().GetMinute()
#define SCREENSHOTS_FOLDER					_T(SCREENSHOTS_FOLDER_T)
#define SCREENSHOTS_FILE_NAME(IDX)			wxString::Format(SCREENSHOTS_FILE_NAME_T, N_YEAR, N_MONTH, N_DAY, N_HOUR, N_MINUTE, IDX)
#define SCREENSHOTS_FILE_FULLNAME(IDX)		wxString::Format(SCREENSHOTS_FILE_FULLNAME_T, N_YEAR, N_MONTH, N_DAY, N_HOUR, N_MINUTE, IDX)

BEGIN_EVENT_TABLE(LCD_DeviceFrame,LCD_FrameBase)
    EVT_CLOSE		(LCD_DeviceFrame::wxEvent_OnClose)
    EVT_KEY_DOWN	(LCD_DeviceFrame::wxEvent_OnKeyPress)
    EVT_TOOL		(wxID_LCD_TOOLBAR_CLEARSCREEN, LCD_DeviceFrame::wxEvent_OnClearScreen)
    EVT_TOOL		(wxID_LCD_TOOLBAR_QUICKSHOTS, LCD_DeviceFrame::wxEvent_OnQuickShot)
    EVT_TOOL		(wxID_LCD_TOOLBAR_COPYSCREENSHOTS, LCD_DeviceFrame::wxEvent_OnCopyScreenshot)
    EVT_TOOL		(wxID_LCD_TOOLBAR_OPENSCREENSHOTSFOLDER, LCD_DeviceFrame::wxEvent_OnOpenScreenshotsFolder)
    EVT_TOOL		(wxID_LCD_TOOLBAR_CONNECTDEVICE, LCD_DeviceFrame::wxEvent_OnDeviceConnect)
    EVT_TOOL		(wxID_LCD_TOOLBAR_SHOWLOGWINDOW, LCD_DeviceFrame::wxEvent_OnShowLogWindow)
    EVT_TOOL		(wxID_LCD_TOOLBAR_SETTINGS, LCD_DeviceFrame::wxEvent_OnSetting)
    EVT_TOOL		(wxID_LCD_TOOLBAR_ABOUT, LCD_DeviceFrame::wxEvent_OnAbout)
    EVT_TOOL		(wxID_LCD_TOOLBAR_EXIT, LCD_DeviceFrame::wxEvent_OnExit)
 	EVT_TIMER		(wxID_LCD_TIMER_RECIVE, LCD_DeviceFrame::wxEvent_OnTimerEvent)
 	EVT_DATA_RECIVED(LCD_DeviceFrame::wxEvent_OnControl)
END_EVENT_TABLE()

LCD_DeviceFrame::LCD_DeviceFrame(LCD_FrameBase* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) :
LCD_FrameBase(parent, id, title, pos, size, style )
{
	SetSizeHints( wxDefaultSize, wxDefaultSize );
	// Create tool bar.
	m_pclsToolbar = CreateLCDToolBar();
	// Create status bar.
	m_pclsStatusBar = CreateStatusBar( 1, wxST_SIZEGRIP, wxID_ANY );
	// Create pixel panel.

	m_pclsPaintPanel = new LCD_ScreenPanel(this, wxID_LCD_PAINT_PANEL);
	m_pclsPaintPanel->RegisterResponseFrame(this);
	m_pclsPaintPanel->SetParameter(&(g_stParameters.Panel));
	m_pclsPaintPanel->ResizeParent();
	m_pclsPaintPanel->SetPosition(0, 0);
	// Create screenshots folder if not existed.
    if(false == wxDirExists(SCREENSHOTS_FOLDER))
	{
        wxMkdir(SCREENSHOTS_FOLDER);
	}

	// Creata timer.
	m_pclsDataReciveTimer = new wxTimer(this, wxID_LCD_TIMER_RECIVE);
	// Create and open serial port.
	m_pclsSerialPort = new SerialPort(4096);
	m_pclsLogFrame = NULL;

	OnDeviceConnect();

	Centre( wxBOTH );
	SetFocus();
	SetIcon(wxIcon(_T("ID_ICON_MAIN_FRAME"), wxBITMAP_TYPE_ICO_RESOURCE));
}

void LCD_DeviceFrame::UpdateStatusBarText(const wxString& strText)
{
	m_pclsStatusBar->SetStatusText(strText);
}

void LCD_DeviceFrame::OnClearScreen(void)
{
	if(NULL != m_pclsPaintPanel)
	{
		m_pclsPaintPanel->CleanPanel();
		m_pclsPaintPanel->RefreshDisplay();
		UpdateStatusBarText(_TRANS_TEXT("Screen cleared."));
	}
}

void LCD_DeviceFrame::OnCopyScreenshot(void)
{
	if(NULL != m_pclsPaintPanel)
	{
		m_pclsPaintPanel->CopyToClipBoard();
		UpdateStatusBarText(_TRANS_TEXT("Screen shot cpoied to clipboard."));
	}
}

void LCD_DeviceFrame::OnQuickShot(void)
{
	uint32_t		uiFileCounter = 1;
	while(true == wxFileExists(SCREENSHOTS_FILE_FULLNAME(uiFileCounter)))
	{
		uiFileCounter++;
	}
	if(true == m_pclsPaintPanel->SaveToFile(SCREENSHOTS_FILE_FULLNAME(uiFileCounter)))
	{
		SetStatusText(wxString::Format(_TRANS_TEXT("Save screen to %s."), SCREENSHOTS_FILE_NAME(uiFileCounter)));
	}
	else
	{
		SetStatusText(_TRANS_TEXT("Save screen failed."));
	}
}

void LCD_DeviceFrame::OnChildWindowClose(wxWindowID iFramrID)
{
	m_pclsLogFrame = NULL;
}

void LCD_DeviceFrame::OnShowLogWindow(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(NULL == m_pclsLogFrame)
	{
		if(true == m_pclsSerialPort->IsOpen())
		{
			m_pclsLogFrame = new LCD_FrameLog(this, wxID_LOG_FRAME, LOG_FRAME_TITLE);
			m_pclsLogFrame->Show();
			SetFocus();
		}
		else
		{
            wxMessageBox(wxString(_TRANS_TEXT("Device is not connected!")), wxString(_TRANS_TEXT("Error")), wxICON_ERROR|wxOK, this);
		}
	}
	else
	{
		m_pclsLogFrame->Close();
	}
}

wxToolBar* LCD_DeviceFrame::CreateLCDToolBar(void)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxToolBar*					pclsNewToolBar;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	pclsNewToolBar				= NULL;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/

	pclsNewToolBar = CreateToolBar(wxTB_HORIZONTAL, wxID_LCD_TOOLBAR);
	if(NULL != pclsNewToolBar)
	{
		pclsNewToolBar->AddTool(wxID_LCD_TOOLBAR_CLEARSCREEN, _TRANS_TEXT("Clean screen"),
												wxBitmap(_T("ID_TOOL_CLEARSCREEN"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxBitmap(_T("ID_TOOL_CLEARSCREEN"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxITEM_NORMAL,
												wxString::Format(_TRANS_TEXT("Clean screen(Ctrl+%s)"), g_arrstShortcutKeyTable[SHORTCUT_KEY_INDEX_CLEARSCREEN].Text),
												_TRANS_TEXT("Clean screen display."));

		pclsNewToolBar->AddTool(wxID_LCD_TOOLBAR_QUICKSHOTS, _TRANS_TEXT("Quick Shot"),
												wxBitmap(_T("ID_TOOL_QUICKSHOTS"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxBitmap(_T("ID_TOOL_QUICKSHOTS"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxITEM_NORMAL,
												wxString::Format(_TRANS_TEXT("Quick Shot(Ctrl+%s)"), g_arrstShortcutKeyTable[SHORTCUT_KEY_INDEX_QUICKSHOTS].Text),
												_TRANS_TEXT("Quick save screen shot to file."));

		pclsNewToolBar->AddTool(wxID_LCD_TOOLBAR_COPYSCREENSHOTS, _TRANS_TEXT("Copy Screenshots"),
												wxBitmap(_T("ID_TOOL_COPYSCREENSHOT"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxBitmap(_T("ID_TOOL_COPYSCREENSHOT"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxITEM_NORMAL,
												wxString::Format(_TRANS_TEXT("Copy Screenshots(Ctrl+%s)"), g_arrstShortcutKeyTable[SHORTCUT_KEY_INDEX_COPYSCREENSHOTS].Text),
												_TRANS_TEXT("Copy screenshots picture to clipboard."));

		pclsNewToolBar->AddTool(wxID_LCD_TOOLBAR_OPENSCREENSHOTSFOLDER, _TRANS_TEXT("Open Screenshots Folder"),
												wxBitmap(_T("ID_TOOL_OPENSCREENSHOTSFOLDER"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxBitmap(_T("ID_TOOL_OPENSCREENSHOTSFOLDER"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxITEM_NORMAL,
												wxString::Format(_TRANS_TEXT("Open Screenshots Folder(Ctrl+%s)"), g_arrstShortcutKeyTable[SHORTCUT_KEY_INDEX_OPENSCREENSHOTSFOLDER].Text),
												_TRANS_TEXT("Open screenshots saved folder."));

		pclsNewToolBar->AddSeparator();

		pclsNewToolBar->AddTool(wxID_LCD_TOOLBAR_CONNECTDEVICE, _TRANS_TEXT("Connect Device"),
												wxBitmap(_T("ID_TOOL_DEVICECONNECT"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxBitmap(_T("ID_TOOL_DEVICECONNECT"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxITEM_NORMAL,
												_TRANS_TEXT("Connect Device"),
												_TRANS_TEXT("Try to connect device."));

		pclsNewToolBar->AddTool(wxID_LCD_TOOLBAR_SHOWLOGWINDOW, _TRANS_TEXT("Show data log"),
												wxBitmap(_T("ID_TOOL_SHOWLOGWINDOW"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxBitmap(_T("ID_TOOL_SHOWLOGWINDOW_DISABLE"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxITEM_NORMAL,
												_TRANS_TEXT("Show Data Log Window"),
												_TRANS_TEXT("Show realtime data log window."));

		pclsNewToolBar->AddSeparator();

		pclsNewToolBar->AddTool(wxID_LCD_TOOLBAR_SETTINGS, _TRANS_TEXT("Setting"),
												wxBitmap(_T("ID_TOOL_SETTING"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxBitmap(_T("ID_TOOL_SETTING"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxITEM_NORMAL,
												_TRANS_TEXT("Setting"),
												_TRANS_TEXT("Setting."));

		pclsNewToolBar->AddTool(wxID_LCD_TOOLBAR_ABOUT, _TRANS_TEXT("About"),
												wxBitmap(_T("ID_TOOL_ABOUT"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxBitmap(_T("ID_TOOL_ABOUT"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxITEM_NORMAL,
												_TRANS_TEXT("About"),
												_TRANS_TEXT("About."));


		pclsNewToolBar->AddSeparator();

		pclsNewToolBar->AddTool(wxID_LCD_TOOLBAR_EXIT, _TRANS_TEXT("Exit"),
												wxBitmap(_T("ID_TOOL_EXIT"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxBitmap(_T("ID_TOOL_EXIT"), wxBITMAP_TYPE_PNG_RESOURCE),
												wxITEM_NORMAL,
												_TRANS_TEXT("Exit(Alt+F4)"),
												_TRANS_TEXT("Exit."));
	}

	pclsNewToolBar->Realize();

	return pclsNewToolBar;
}

void LCD_DeviceFrame::OnSetting(void)
{
    LCD_DialogSettings*	pclsSettingDlg;

	// Create setting dialog object.
	pclsSettingDlg = new LCD_DialogSettings(this);
	// Close serial before setting.
	CloseSerialPort();
	// Load parameter data and show window as a dialog.
	uint32_t uiResult = pclsSettingDlg->ShowDialog(&g_stParameters);
	if(5100 == uiResult)
	{
		// Reset pixel panel if click OK.
        if(NULL != m_pclsPaintPanel)
		{
			SaveParameter(&g_stParameters);
			m_pclsPaintPanel->SetParameter(&g_stParameters.Panel);
			m_pclsPaintPanel->ResizeParent();
			m_pclsPaintPanel->CleanPanel();
			m_pclsPaintPanel->RefreshDisplay();
		}
		OnDeviceConnect();
	}
	if(5101 == uiResult)
	{
		//wxMessageBox(wxT("Pressed Cancel."));
	}

	delete pclsSettingDlg;
}

void LCD_DeviceFrame::About(void)
{
	OEMDialog	clsAboutDialog(this);
	clsAboutDialog.ShowModal();
	//wxMessageBox(wxString::Format(_TRANS_TEXT("LCD screen simulator.")));
}

void LCD_DeviceFrame::OnOpenScreenShotsFolder(void)
{
	wxExecute(wxString::Format(_T("explorer %s\\%s"), wxGetCwd(), _T(SCREENSHOTS_FOLDER_T)));
	printf("Load language file failed.");
}

void LCD_DeviceFrame::OnDeviceConnect(void)
{
	// Close port and change help text to prepare to open.
	if(true == m_pclsSerialPort->IsOpen())
	{
        CloseSerialPort();
        m_pclsToolbar->SetToolShortHelp(wxID_LCD_TOOLBAR_CONNECTDEVICE, _TRANS_TEXT("Connect Device"));
        m_pclsToolbar->SetToolLongHelp(wxID_LCD_TOOLBAR_CONNECTDEVICE, _TRANS_TEXT("Try to connect device."));
        m_pclsToolbar->SetToolNormalBitmap(wxID_LCD_TOOLBAR_CONNECTDEVICE, wxBitmap("ID_TOOL_DEVICEDISCONNECT", wxBITMAP_TYPE_PNG_RESOURCE));
        m_pclsToolbar->EnableTool(wxID_LCD_TOOLBAR_SHOWLOGWINDOW, false);
	}
	// Try to open port and change help text to prepare to close;
	else
	{
		bool 		bResult;

		bResult = OpenSerialPort();
		if(true == bResult)
		{
			m_pclsToolbar->SetToolShortHelp(wxID_LCD_TOOLBAR_CONNECTDEVICE, _TRANS_TEXT("Disconnect Device"));
			m_pclsToolbar->SetToolLongHelp(wxID_LCD_TOOLBAR_CONNECTDEVICE, _TRANS_TEXT("Disconnect device and close log window if opened."));
			m_pclsToolbar->SetToolNormalBitmap(wxID_LCD_TOOLBAR_CONNECTDEVICE, wxBitmap("ID_TOOL_DEVICECONNECT", wxBITMAP_TYPE_PNG_RESOURCE));
			m_pclsToolbar->EnableTool(wxID_LCD_TOOLBAR_SHOWLOGWINDOW, true);
		}
		else
		{
			m_pclsToolbar->SetToolShortHelp(wxID_LCD_TOOLBAR_CONNECTDEVICE, _TRANS_TEXT("Connect Device"));
			m_pclsToolbar->SetToolLongHelp(wxID_LCD_TOOLBAR_CONNECTDEVICE, _TRANS_TEXT("Try to connect device."));
			m_pclsToolbar->SetToolNormalBitmap(wxID_LCD_TOOLBAR_CONNECTDEVICE, wxBitmap("ID_TOOL_DEVICEDISCONNECT", wxBITMAP_TYPE_PNG_RESOURCE));
			m_pclsToolbar->EnableTool(wxID_LCD_TOOLBAR_SHOWLOGWINDOW, false);
		}
	}
}

/*************************************************************************/
/** Function Name:	OnTimerEvent										**/
/** Purpose:		Serial port recive data process.					**/
/** Resources:		None												**/
/** Params:																**/
/**	@[in]event:			Event data.										**/
/** Return:			None.												**/
/*************************************************************************/
void LCD_DeviceFrame::OnTimerEvent(wxTimerEvent& event)
{
	static uint8_t			arrControlData[SERIAL_EVENT_DATA_SIZE];
	static size_t			uiReadDataSize;
	static size_t			uiProcessDataCount = 0;
	static size_t			uiTimeOutCounter = 0;
	CLCDControlEvent		clsLCDControlEvent(wxEVT_ON_DATA_RECIVED);

	if(1 == m_pclsSerialPort->IsOpen())
	{
		if((uiReadDataSize > 0) && (uiReadDataSize < SERIAL_EVENT_DATA_SIZE))
		{
			size_t		uiAppendSize;
			uiAppendSize = m_pclsSerialPort->Read(((char*)arrControlData)+uiReadDataSize, (SERIAL_EVENT_DATA_SIZE-uiReadDataSize));

			if((SERIAL_EVENT_DATA_SIZE-uiReadDataSize) == uiAppendSize)
			{
				clsLCDControlEvent.SetCommandID(arrControlData[SERIAL_EVENT_DATA_INDEX_COMMAND]);
				clsLCDControlEvent.SetData(arrControlData[SERIAL_EVENT_DATA_INDEX_VALUE_H], arrControlData[SERIAL_EVENT_DATA_INDEX_VALUE_L]);
				if(NULL != m_pclsLogFrame)
				{
					wxPostEvent(m_pclsLogFrame->GetEventHandler(), clsLCDControlEvent);
				}
				if(NULL != m_pclsPaintPanel)
				{
					wxPostEvent(m_pclsPaintPanel->GetEventHandler(), clsLCDControlEvent);
				}
				uiReadDataSize = 0;
			}
			else
			{
				uiReadDataSize += uiAppendSize;
			}
		}
		else
		{
			uiReadDataSize = 0;
		}

		if(uiReadDataSize == 0)
		{
			while(uiProcessDataCount < SERIAL_EVENT_DATA_READ_PEREVENT)
			{
				uiReadDataSize = m_pclsSerialPort->Read((char*)arrControlData, SERIAL_EVENT_DATA_SIZE);
				if(SERIAL_EVENT_DATA_SIZE == uiReadDataSize)
				{
					clsLCDControlEvent.SetCommandID(arrControlData[SERIAL_EVENT_DATA_INDEX_COMMAND]);
					clsLCDControlEvent.SetData(arrControlData[SERIAL_EVENT_DATA_INDEX_VALUE_H], arrControlData[SERIAL_EVENT_DATA_INDEX_VALUE_L]);
					if(NULL != m_pclsLogFrame)
					{
						wxPostEvent(m_pclsLogFrame->GetEventHandler(), clsLCDControlEvent);
					}
					if(NULL != m_pclsPaintPanel)
					{
						wxPostEvent(m_pclsPaintPanel->GetEventHandler(), clsLCDControlEvent);
					}
					uiProcessDataCount++;
				}
				else
				{
					break;
				}
			}
			uiProcessDataCount = 0;
		}
		else
		{
			uiTimeOutCounter ++;
			if(uiTimeOutCounter < 10)
			{
				uiReadDataSize = 0;
			}
		}
	}
}

void LCD_DeviceFrame::AppendLog(wxString strLog)
{
	if(NULL != m_pclsLogFrame)
	{
		m_pclsLogFrame->AppendText(strLog);
	}
}

bool LCD_DeviceFrame::OpenSerialPort(void)
{
	bool 		bResult;
	if(false == m_pclsSerialPort->IsOpen())
    {
    	((SerialPort*)m_pclsSerialPort)->SetBaudrate(g_stParameters.Port.Baudrate);
        if(0 != m_pclsSerialPort->Open(g_stParameters.Port.PortName, NULL))
        {
        	wxMessageBox(wxString::Format(_TRANS_TEXT("%s open failed."), g_stParameters.Port.PortName), wxString(_TRANS_TEXT("Error")), wxOK|wxICON_ERROR, this);
            UpdateStatusBarText(wxString::Format(_TRANS_TEXT("%s open failed."), g_stParameters.Port.PortName));
            bResult = false;
        }
        else
        {
            UpdateStatusBarText(wxString::Format(_TRANS_TEXT("%s open successfully, baudrate is %u."), g_stParameters.Port.PortName, g_stParameters.Port.Baudrate));
            m_pclsDataReciveTimer->Start(10);
            bResult = true;
        }
    }
    else
	{
		// Port is already opened.
		bResult = true;
	}

    return bResult;
}

void LCD_DeviceFrame::CloseSerialPort(void)
{
	if(true == m_pclsSerialPort->IsOpen())
    {
    	m_pclsSerialPort->Close();
    	m_pclsDataReciveTimer->Stop();
    	m_pclsToolbar->EnableTool(wxID_LCD_TOOLBAR_SHOWLOGWINDOW, false);
    	UpdateStatusBarText(wxString::Format(_TRANS_TEXT("%s is closed."), g_stParameters.Port.PortName));
    }
}

void LCD_DeviceFrame::OnKeyPress(wxKeyEvent& event)
{
	int			iKeyCode;
    if(true == event.ControlDown())
	{
		iKeyCode = event.GetKeyCode();
        if(iKeyCode == g_arrstShortcutKeyTable[SHORTCUT_KEY_INDEX_CLEARSCREEN].KeyCode)
		{
			OnClearScreen();
		}
		else if(iKeyCode == g_arrstShortcutKeyTable[SHORTCUT_KEY_INDEX_COPYSCREENSHOTS].KeyCode)
		{
			OnCopyScreenshot();
		}
		else if(iKeyCode == g_arrstShortcutKeyTable[SHORTCUT_KEY_INDEX_QUICKSHOTS].KeyCode)
		{
			OnQuickShot();
		}
		else if(iKeyCode == g_arrstShortcutKeyTable[SHORTCUT_KEY_INDEX_OPENSCREENSHOTSFOLDER].KeyCode)
		{
			OnOpenScreenShotsFolder();
		}
	}
}

void LCD_DeviceFrame::OnControl(CLCDControlEvent& event)
{
	//No action.
}

void LCD_DeviceFrame::OnResopnse(uint8_t uiCommand, uint16_t uiData)
{
	uint8_t			arrControlData[SERIAL_EVENT_DATA_SIZE];
    if(true == m_pclsSerialPort->IsOpen())
	{
		arrControlData[SERIAL_EVENT_DATA_INDEX_COMMAND] = uiCommand;
		arrControlData[SERIAL_EVENT_DATA_INDEX_VALUE_L] = (uint8_t)(uiData&0xFF);
		arrControlData[SERIAL_EVENT_DATA_INDEX_VALUE_H] = (uint8_t)(uiData>>8);
	}
	if(1 == m_pclsSerialPort->IsOpen())
	{
		m_pclsSerialPort->Write((char*)arrControlData, SERIAL_EVENT_DATA_SIZE);
	}
}

LCD_DeviceFrame::~LCD_DeviceFrame()
{
	if(NULL != m_pclsLogFrame)
	{
		m_pclsLogFrame->Close();
		delete m_pclsLogFrame;
		m_pclsLogFrame = NULL;
	}
	if(NULL != m_pclsSerialPort)
	{
		if(1 == m_pclsSerialPort->IsOpen())
		{
			m_pclsSerialPort->Close();
			m_pclsDataReciveTimer->Stop();
		}
		delete m_pclsSerialPort;
		delete m_pclsDataReciveTimer;
	}
}

