///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "LCD_DialogAbout.h"

const wxString OEM_DEFAULT_INTRODUCTION(wxT(
"This is a LCD display simulator using serial communication.\n\
Please get the latest updates at the following address:\n\
https://git.oschina.net/Polarix/lcdsimulator\n\
Welcome your feedback or comments and bugs."
));

const wxString OEM_DEFAULT_DEVELOPERS(wxT(
"Polarix develope and released in 2017-7-13.\n\
QQ/WeChat: 326684221\n\
E-Mail: 326684221@qq.com"
));

///////////////////////////////////////////////////////////////////////////
#define OEM_IMAGE_FILE_PATH					wxT(".\\OEM\\Image.png")
#define OEM_TEXT_INTRODUCTION_FILE_PATH		wxT(".\\OEM\\Introduction.txt")
#define OEM_TEXT_DEVELOPERS_FILE_PATH		wxT(".\\OEM\\Developers.txt")
///////////////////////////////////////////////////////////////////////////

OEMDialog::OEMDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* pclsSizerAbout;
	pclsSizerAbout = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* pclsSizerOEMImage;
	pclsSizerOEMImage = new wxBoxSizer( wxVERTICAL );

	m_pclsBitmapOEM = new wxStaticBitmap( this, wxID_ANY, wxBitmap(_T("ID_IMAGE_ABOUT"), wxBITMAP_TYPE_PNG_RESOURCE), wxDefaultPosition, wxSize( 480,160 ), 0 );
	pclsSizerOEMImage->Add( m_pclsBitmapOEM, 0, wxALL, 5 );


	pclsSizerAbout->Add( pclsSizerOEMImage, 1, wxEXPAND, 5 );

	wxBoxSizer* pclsSizerInfo;
	pclsSizerInfo = new wxBoxSizer( wxVERTICAL );

	m_pclsNotebookInfo = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxSize( -1,160 ), 0 );
	m_pclsPanelIntroduction = new wxPanel( m_pclsNotebookInfo, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* pclsSizerIntroductionText;
	pclsSizerIntroductionText = new wxBoxSizer( wxVERTICAL );

	m_pcslTextCtrlIntroduction = new wxTextCtrl( m_pclsPanelIntroduction, wxID_ANY, OEM_DEFAULT_INTRODUCTION, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
	pclsSizerIntroductionText->Add( m_pcslTextCtrlIntroduction, 0, wxEXPAND, 5 );


	m_pclsPanelIntroduction->SetSizer( pclsSizerIntroductionText );
	m_pclsPanelIntroduction->Layout();
	pclsSizerIntroductionText->Fit( m_pclsPanelIntroduction );
	m_pclsNotebookInfo->AddPage( m_pclsPanelIntroduction, _TRANS_TEXT("About LCDSimulator"), true );
	m_pclsPanelDeveloper = new wxPanel( m_pclsNotebookInfo, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* pclsSizerDeveloperInfo;
	pclsSizerDeveloperInfo = new wxBoxSizer( wxVERTICAL );

	m_pcslTextCtrlDevelopers = new wxTextCtrl( m_pclsPanelDeveloper, wxID_ANY, OEM_DEFAULT_DEVELOPERS, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
	pclsSizerDeveloperInfo->Add( m_pcslTextCtrlDevelopers, 0, wxEXPAND, 5 );

	m_pclsPanelDeveloper->SetSizer( pclsSizerDeveloperInfo );
	m_pclsPanelDeveloper->Layout();
	pclsSizerDeveloperInfo->Fit( m_pclsPanelDeveloper );
	m_pclsNotebookInfo->AddPage( m_pclsPanelDeveloper, _TRANS_TEXT("Developers"), false );

	pclsSizerInfo->Add( m_pclsNotebookInfo, 0, wxEXPAND | wxALL, 5 );

	pclsSizerAbout->Add( pclsSizerInfo, 0, wxEXPAND, 5 );

	this->SetSizer( pclsSizerAbout );
	this->Layout();
	pclsSizerAbout->Fit( this );

	this->Centre( wxBOTH );

	Initialize();
}

OEMDialog::~OEMDialog()
{
}

void OEMDialog::Initialize(void)
{
	if(true == wxFile::Exists(OEM_IMAGE_FILE_PATH))
	{
		wxBitmap	clsOEMImage;
		if(true == clsOEMImage.LoadFile(OEM_IMAGE_FILE_PATH, wxBITMAP_TYPE_PNG))
		{
			m_pclsBitmapOEM->SetBitmap(clsOEMImage);
		}
	}

	if(true == wxFile::Exists(OEM_TEXT_INTRODUCTION_FILE_PATH))
	{
        m_pcslTextCtrlIntroduction->LoadFile(OEM_TEXT_INTRODUCTION_FILE_PATH);
	}

	if(true == wxFile::Exists(OEM_TEXT_DEVELOPERS_FILE_PATH))
	{
        m_pcslTextCtrlDevelopers->LoadFile(OEM_TEXT_DEVELOPERS_FILE_PATH);
	}
}
