///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "LCD_Common.h"
#include "LCD_DialogSetting.h"

#define	LCD_SETTING_PREVIEW_BORDER		(5)

///////////////////////////////////////////////////////////////////////////

const size_t arruiPreviewData[] =
{	0x00,0x00,0x00,0x00,0xF0,0xF0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xC0,0xE0,0x70,0x30,0x30,
	0x70,0xE0,0xC0,0x00,0x00,0xF0,0xF0,0x30,0x30,0x70,0xE0,0xC0,0x80,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00,0x01,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x0F,0x0F,0x0C,0x0C,0x0C,0x0C,0x0C,0x0C,0x00,0x03,0x07,0x0E,0x0C,0x0C,
	0x0E,0x07,0x03,0x00,0x00,0x0F,0x0F,0x0C,0x0C,0x0E,0x07,0x03,0x01,0x00,0x00,0x00,0x00,0x00};

BEGIN_EVENT_TABLE(LCD_DialogSettings,wxDialog)
EVT_UPDATE_UI(wxID_SETTING_DIALOG, LCD_DialogSettings::wxEvent_OnUpdateUI)
EVT_SPINCTRL(wxID_SETTING_SPINCTRL_COLUMNS, LCD_DialogSettings::wxEvent_OnSpinCtrlValueUpdate)
EVT_SPINCTRL(wxID_SETTING_SPINCTRL_PAGES, LCD_DialogSettings::wxEvent_OnSpinCtrlValueUpdate)
EVT_SPINCTRL(wxID_SETTING_SPINCTRL_PIXELSIZE, LCD_DialogSettings::wxEvent_OnSpinCtrlValueUpdate)
EVT_BUTTON(wxID_SETTING_BUTTON_BACKGROUNDCOLOR, LCD_DialogSettings::wxEvent_OnColorSetChanged)
EVT_BUTTON(wxID_SETTING_BUTTON_PIXELCOLOR, LCD_DialogSettings::wxEvent_OnColorSetChanged)
EVT_BUTTON(wxID_SETTING_BUTTON_EDGECOLOR, LCD_DialogSettings::wxEvent_OnColorSetChanged)
EVT_BUTTON(wxID_SETTING_BUTTON_GRIDCOLOR, LCD_DialogSettings::wxEvent_OnColorSetChanged)
EVT_BUTTON(wxID_SETTING_BUTTON_IMPORTFILE, LCD_DialogSettings::wxEvent_OnLoadConfigFile)
EVT_COMBOBOX_CLOSEUP(wxID_COMBOBOX_PORT, LCD_DialogSettings::wxEvent_OnPortSettingChanged)
EVT_CHECKBOX(wxID_SETTING_CHECK_GRIDENABLE, LCD_DialogSettings::wxEvent_OnGridEnable)
END_EVENT_TABLE()
LCD_DialogSettings::LCD_DialogSettings( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) :
	wxDialog( parent, id, title, pos, size, style ),
	m_clsRtnDevicesInfoList()
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	m_pclsSizer_DialogMain = new wxBoxSizer( wxVERTICAL );

	m_pclsStaticBoxSizer_ComPort = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _TRANS_TEXT("Port settings") ), wxVERTICAL );

	m_pclsSizer_ComPort = new wxBoxSizer( wxHORIZONTAL );

	m_pclsStaticText_ComPort = new wxStaticText( m_pclsStaticBoxSizer_ComPort->GetStaticBox(), wxID_ANY, _TRANS_TEXT("Port:"), wxDefaultPosition, wxDefaultSize, wxST_NO_AUTORESIZE );
	m_pclsStaticText_ComPort->Wrap( -1 );
	m_pclsSizer_ComPort->Add( m_pclsStaticText_ComPort, 0, wxALIGN_CENTER|wxALL, 5 );

	// Get all useful serial port
	GetAvailablePorts(m_clsRtnDevicesInfoList);
	m_pclsComboBox_ComPort = new wxComboBox( m_pclsStaticBoxSizer_ComPort->GetStaticBox(), wxID_COMBOBOX_PORT, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), 0, NULL, wxCB_READONLY );
	{
		vector<PortInfo>::iterator clsIter = m_clsRtnDevicesInfoList.begin();
		m_pclsComboBox_ComPort->Clear();
		while(clsIter != m_clsRtnDevicesInfoList.end())
		{
			PortInfo stDevice = *clsIter++;
			m_pclsComboBox_ComPort->Append(wxString::Format("%s - %s", stDevice.PortName, stDevice.DeviceName));
		}
		//m_pclsComboBox_ComPort->SetSelection(uiSelectIndex);
	}
	m_pclsSizer_ComPort->Add( m_pclsComboBox_ComPort, 1, wxALL, 5 );

	m_pclsStaticText_Baudrate = new wxStaticText( m_pclsStaticBoxSizer_ComPort->GetStaticBox(), wxID_ANY, _TRANS_TEXT("Baudrate:"), wxDefaultPosition, wxDefaultSize, wxST_NO_AUTORESIZE );
	m_pclsStaticText_Baudrate->Wrap( -1 );
	m_pclsSizer_ComPort->Add( m_pclsStaticText_Baudrate, 0, wxALIGN_CENTER|wxALL, 5 );

	m_pclsComboBox_Baudrate = new wxComboBox( m_pclsStaticBoxSizer_ComPort->GetStaticBox(), wxID_COMBOBOX_BAUDRATE, wxT("1200"), wxDefaultPosition, wxSize( -1,-1 ), 0, NULL, 0 );
	m_pclsComboBox_Baudrate->Append( wxT("1200") );
	m_pclsComboBox_Baudrate->Append( wxT("2400") );
	m_pclsComboBox_Baudrate->Append( wxT("4800") );
	m_pclsComboBox_Baudrate->Append( wxT("9600") );
	m_pclsComboBox_Baudrate->Append( wxT("19200") );
	m_pclsComboBox_Baudrate->Append( wxT("38400") );
	m_pclsComboBox_Baudrate->Append( wxT("57600") );
	m_pclsComboBox_Baudrate->Append( wxT("115200") );
	m_pclsComboBox_Baudrate->SetSelection( 0 );
	m_pclsSizer_ComPort->Add( m_pclsComboBox_Baudrate, 0, wxALL, 5 );


	m_pclsStaticBoxSizer_ComPort->Add( m_pclsSizer_ComPort, 0, wxEXPAND, 5 );


	m_pclsSizer_DialogMain->Add( m_pclsStaticBoxSizer_ComPort, 0, wxALL|wxEXPAND, 5 );

	m_pclsStaticBoxSizer_Appearance = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _TRANS_TEXT("Appearance") ), wxHORIZONTAL );

	m_pclsStaticText_Columns = new wxStaticText( m_pclsStaticBoxSizer_Appearance->GetStaticBox(), wxID_ANY, _TRANS_TEXT("Columns:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pclsStaticText_Columns->Wrap( -1 );
	m_pclsStaticBoxSizer_Appearance->Add( m_pclsStaticText_Columns, 0, wxALIGN_CENTER|wxALL, 5 );

	m_pclsSpinCtrl_Columns = new wxSpinCtrl( m_pclsStaticBoxSizer_Appearance->GetStaticBox(), wxID_SETTING_SPINCTRL_COLUMNS, wxEmptyString, wxDefaultPosition, wxSize( 75,-1 ), wxSP_ARROW_KEYS, 32, 320, 128 );
	m_pclsStaticBoxSizer_Appearance->Add( m_pclsSpinCtrl_Columns, 0, wxALL, 5 );

	m_pclsStaticText_Pages = new wxStaticText( m_pclsStaticBoxSizer_Appearance->GetStaticBox(), wxID_ANY, _TRANS_TEXT("Pages:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pclsStaticText_Pages->Wrap( -1 );
	m_pclsStaticBoxSizer_Appearance->Add( m_pclsStaticText_Pages, 0, wxALIGN_CENTER|wxALL, 5 );

	m_pclsSpinCtrl_Pages = new wxSpinCtrl( m_pclsStaticBoxSizer_Appearance->GetStaticBox(), wxID_SETTING_SPINCTRL_PAGES, wxEmptyString, wxDefaultPosition, wxSize( 75,-1 ), wxSP_ARROW_KEYS, 4, 20, 8 );
	m_pclsStaticBoxSizer_Appearance->Add( m_pclsSpinCtrl_Pages, 0, wxALL, 5 );

	m_pclsStaticText_PixelSize = new wxStaticText( m_pclsStaticBoxSizer_Appearance->GetStaticBox(), wxID_ANY, _TRANS_TEXT("Pixel size:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pclsStaticText_PixelSize->Wrap( -1 );
	m_pclsStaticBoxSizer_Appearance->Add( m_pclsStaticText_PixelSize, 0, wxALIGN_CENTER|wxALL, 5 );

	m_pclsSpinCtrl_PixelSize = new wxSpinCtrl( m_pclsStaticBoxSizer_Appearance->GetStaticBox(), wxID_SETTING_SPINCTRL_PIXELSIZE, wxEmptyString, wxDefaultPosition, wxSize( 75,-1 ), wxSP_ARROW_KEYS, 1, 5, 2 );
	m_pclsStaticBoxSizer_Appearance->Add( m_pclsSpinCtrl_PixelSize, 0, wxALL, 5 );


	m_pclsSizer_DialogMain->Add( m_pclsStaticBoxSizer_Appearance, 0, wxALL|wxEXPAND, 5 );

	m_pclsStaticBoxSizer_Colors = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _TRANS_TEXT("Colors") ), wxHORIZONTAL );

	m_pclsSizer_ColorSettings = new wxBoxSizer( wxVERTICAL );

	m_pclsButtonScreenBackColor = new wxButton( m_pclsStaticBoxSizer_Colors->GetStaticBox(), wxID_SETTING_BUTTON_BACKGROUNDCOLOR, _TRANS_TEXT("Panel"), wxDefaultPosition, wxSize( 120,25 ), 0 );
	m_pclsSizer_ColorSettings->Add( m_pclsButtonScreenBackColor, 0, wxALL, 5 );
	m_pclsButtonScreenPixelColor = new wxButton( m_pclsStaticBoxSizer_Colors->GetStaticBox(), wxID_SETTING_BUTTON_PIXELCOLOR, _TRANS_TEXT("Pixel"), wxDefaultPosition, wxSize( 120,25 ), 0 );
	m_pclsSizer_ColorSettings->Add( m_pclsButtonScreenPixelColor, 0, wxALL, 5 );
	m_pclsButtonScreenEdgeColor = new wxButton( m_pclsStaticBoxSizer_Colors->GetStaticBox(), wxID_SETTING_BUTTON_EDGECOLOR, _TRANS_TEXT("Edge"), wxDefaultPosition, wxSize( 120,25 ), 0 );
	m_pclsSizer_ColorSettings->Add( m_pclsButtonScreenEdgeColor, 0, wxALL, 5 );
	m_pclsCheckBoxEnableGrid = new wxCheckBox( m_pclsStaticBoxSizer_Colors->GetStaticBox(), wxID_SETTING_CHECK_GRIDENABLE, _TRANS_TEXT("Enable Grid."), wxDefaultPosition, wxSize( 120,20 ), 0 );
	m_pclsSizer_ColorSettings->Add( m_pclsCheckBoxEnableGrid, 0, wxALL, 5 );
	m_pclsButtonScreenGridColor = new wxButton( m_pclsStaticBoxSizer_Colors->GetStaticBox(), wxID_SETTING_BUTTON_GRIDCOLOR, _TRANS_TEXT("Grid"), wxDefaultPosition, wxSize( 120,25 ), 0 );
	m_pclsSizer_ColorSettings->Add( m_pclsButtonScreenGridColor, 0, wxALL, 5 );

	m_pclsStaticBoxSizer_Colors->Add( m_pclsSizer_ColorSettings, 0, 0, 5 );

	/* Add preview control */
	m_pclsSizerPreview = new wxBoxSizer( wxVERTICAL );

	m_pclsPanelPreview = new LCD_ScreenPanel(m_pclsStaticBoxSizer_Colors->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);

	m_pclsSizerPreview->Add( m_pclsPanelPreview, 1, wxEXPAND | wxALL, LCD_SETTING_PREVIEW_BORDER );
	m_pclsStaticBoxSizer_Colors->Add( m_pclsSizerPreview, 1, wxEXPAND, 5 );

	m_pclsSizer_DialogMain->Add( m_pclsStaticBoxSizer_Colors, 1, wxALL|wxEXPAND, 5 );

	m_pclsSizer_DialogButtons = new wxBoxSizer( wxHORIZONTAL );

	// m_pclsButtonImportConfigFile = new wxButton( this, wxID_SETTING_BUTTON_IMPORTFILE, wxT("Import color configuration"), wxDefaultPosition, wxDefaultSize, 0 );
	// m_pclsSizer_DialogButtons->Add( m_pclsButtonImportConfigFile, 0, wxALL, 5 );

	m_pclsStdDialodSizer_DialogButtonsSizer = new wxStdDialogButtonSizer();
	m_pclsStdDialodSizer_DialogButtonsSizerOK = new wxButton(this, wxID_OK, _TRANS_TEXT("OK"));
	m_pclsStdDialodSizer_DialogButtonsSizer->AddButton( m_pclsStdDialodSizer_DialogButtonsSizerOK );
	m_pclsStdDialodSizer_DialogButtonsSizerCancel = new wxButton(this, wxID_CANCEL, _TRANS_TEXT("Cancel"));
	m_pclsStdDialodSizer_DialogButtonsSizer->AddButton( m_pclsStdDialodSizer_DialogButtonsSizerCancel );
	m_pclsStdDialodSizer_DialogButtonsSizer->Realize();

	m_pclsSizer_DialogButtons->Add( m_pclsStdDialodSizer_DialogButtonsSizer, 1, wxBOTTOM|wxTOP, 5 );


	//m_pclsSizer_DialogMain->Add( m_pclsSizer_DialogButtons, 0, wxEXPAND, 5 );
	m_pclsSizer_DialogMain->Add( m_pclsSizer_DialogButtons, 0, wxALIGN_CENTER_HORIZONTAL, 5 );

	this->SetSizer( m_pclsSizer_DialogMain );
	this->Layout();

	this->Centre( wxBOTH );

	m_bPreviewRefreshed = false;
}

int LCD_DialogSettings::ShowDialog(SimulatorParameter* pstParameterData)
{
	int			iDialogResult;
	if(NULL != pstParameterData)
	{
		// Create parameter data copy.
		memcpy(&m_stSettingParameter, pstParameterData, sizeof(SimulatorParameter));

		// Set controls value.
		vector<PortInfo>::iterator clsIter = m_clsRtnDevicesInfoList.begin();
		size_t					uiSelectIndex = 0;
		while(clsIter != m_clsRtnDevicesInfoList.end())
		{
			PortInfo stDevice = *clsIter++;
			if(strcmp(stDevice.PortName, m_stSettingParameter.Port.PortName) == 0)
			{
				break;
			}
			else
			{
				uiSelectIndex++;
			}
		}
		if(m_pclsComboBox_ComPort->GetCount() == uiSelectIndex)
		{
			m_pclsComboBox_ComPort->SetSelection(0);
		}
		else
		{
			m_pclsComboBox_ComPort->SetSelection(uiSelectIndex);
		}
		m_pclsComboBox_Baudrate->SetValue(wxString::Format(wxT("%u"), m_stSettingParameter.Port.Baudrate));
		m_pclsSpinCtrl_Columns->SetValue(m_stSettingParameter.Panel.Size.Columns);
		m_pclsSpinCtrl_Pages->SetValue(m_stSettingParameter.Panel.Size.Pages);
		m_pclsSpinCtrl_PixelSize->SetValue(m_stSettingParameter.Panel.Size.PixelSize);
		m_pclsCheckBoxEnableGrid->SetValue(m_stSettingParameter.Panel.EnableGrid);
		OnPortSet();
		// Update preview panel.
		UpdatePreviewPanel();
		// Show as a Popup dialog.
		iDialogResult = ShowModal();

		if(5100 == iDialogResult)
		{
			long			lBaudrate;
			// Save color and size.
			m_stSettingParameter.Panel.Size.Columns = m_pclsSpinCtrl_Columns->GetValue();
			m_stSettingParameter.Panel.Size.Pages = m_pclsSpinCtrl_Pages->GetValue();
			m_stSettingParameter.Panel.Size.PixelSize = m_pclsSpinCtrl_PixelSize->GetValue();
			m_stSettingParameter.Panel.EnableGrid = m_pclsCheckBoxEnableGrid->GetValue();
			// Save port setting
			strcpy(	m_stSettingParameter.Port.PortName,
					m_pclsComboBox_ComPort->GetStringSelection().Left(m_pclsComboBox_ComPort->GetStringSelection().Find(' '))
					);
			m_pclsComboBox_Baudrate->GetValue().ToLong(&lBaudrate);
			m_stSettingParameter.Port.Baudrate = (uint32_t)lBaudrate;
			memcpy(pstParameterData, &m_stSettingParameter, sizeof(SimulatorParameter));
		}
	}
	else
	{
		iDialogResult = 0;
	}
	return iDialogResult;
}

void LCD_DialogSettings::OnUpdateUI(wxUpdateUIEvent& event)
{
	m_pclsPanelPreview->OnPaint();
}

void LCD_DialogSettings::UpdatePreviewPanel(void)
{
	uint32_t					uiPreviewWidth, uiPreviewHeight;
	PixelPanelParameter			stPanelParameter;

	stPanelParameter.EdgeColor.RGBA =	m_stSettingParameter.Panel.Color.Edge.RGBA;
	stPanelParameter.PanelColor.RGBA =	m_stSettingParameter.Panel.Color.Panel.RGBA;
	stPanelParameter.PixelColor.RGBA =	m_stSettingParameter.Panel.Color.Pixel.RGBA;
	stPanelParameter.GridColor.RGBA =	m_stSettingParameter.Panel.Color.Grid.RGBA;

	stPanelParameter.EnableGrid = 		m_stSettingParameter.Panel.EnableGrid;
	uiPreviewWidth =					m_pclsSizerPreview->GetSize().GetWidth()-(LCD_SETTING_PREVIEW_BORDER*2);
	uiPreviewHeight =					m_pclsSizerPreview->GetSize().GetHeight()-(LCD_SETTING_PREVIEW_BORDER*2);

	stPanelParameter.PixelSize =				m_pclsSpinCtrl_PixelSize->GetValue();
	stPanelParameter.HorizontalPixelNumber =	(uiPreviewWidth-m_stSettingParameter.Panel.Size.EdgeWidth*2)/stPanelParameter.PixelSize;
	stPanelParameter.VerticalPixelNumber =		(uiPreviewHeight-m_stSettingParameter.Panel.Size.EdgeWidth*2)/stPanelParameter.PixelSize;
	stPanelParameter.EdgeWidth =				m_stSettingParameter.Panel.Size.EdgeWidth;

	// Recalculate preview picture size.
	m_pclsPanelPreview->SetSize(wxSize(uiPreviewWidth, uiPreviewHeight));

	// Set preview picture data.
	m_pclsPanelPreview->SetParameter(&stPanelParameter);
	m_pclsPanelPreview->CleanPanel();

	// Fill preview data.
	for(uint32_t i_Page=0; i_Page<3; i_Page++)
	{
		m_pclsPanelPreview->SetPage(i_Page);
		m_pclsPanelPreview->SetColumn(0);
		for(uint32_t i_Column=0; i_Column<36; i_Column++)
		{
			m_pclsPanelPreview->SetData(arruiPreviewData[i_Page*36+i_Column]);
		}
	}
	m_pclsPanelPreview->RefreshDisplay();
}

void LCD_DialogSettings::UpdatePreviewColor(wxCommandEvent& event)
{
	wxColourData clsColourData;
	wxColour clsColour = wxColor(0,0,0);
	clsColourData.SetColour(clsColour);
	clsColourData.SetChooseFull(true);
	wxColourDialog *pclsDialog = new wxColourDialog(this, &clsColourData);
	if (wxID_OK == pclsDialog->ShowModal())
	{
		clsColourData = pclsDialog->GetColourData();
		switch(event.GetId())
		{
			case wxID_SETTING_BUTTON_BACKGROUNDCOLOR:
			{
				m_stSettingParameter.Panel.Color.Panel.RGBA = clsColourData.GetColour().GetRGBA();
				break;
			}
			case wxID_SETTING_BUTTON_PIXELCOLOR:
			{
				m_stSettingParameter.Panel.Color.Pixel.RGBA = clsColourData.GetColour().GetRGBA();
				break;
			}
			case wxID_SETTING_BUTTON_EDGECOLOR:
			{
				m_stSettingParameter.Panel.Color.Edge.RGBA = clsColourData.GetColour().GetRGBA();
				break;
			}
			case wxID_SETTING_BUTTON_GRIDCOLOR:
			{
				m_stSettingParameter.Panel.Color.Grid.RGBA = clsColourData.GetColour().GetRGBA();
				break;
			}
		}
		UpdatePreviewPanel();
	}
	pclsDialog->Destroy();
}

void LCD_DialogSettings::LoadConfigFormFile(void)
{
	wxFileDialog *pclsDialog = new wxFileDialog(this, _TRANS_TEXT("Select a configuration file"), "", "",
                                        _TRANS_TEXT("LCD Simulator Configuration(*.xml)|*.xml"),
                                        wxFD_OPEN, wxDefaultPosition);
	if (wxID_OK == pclsDialog->ShowModal())
	{
		wxString	strFilePath = pclsDialog->GetFilename();
		SimulatorParameter	stConfigData;
		ReadConfigFile(strFilePath, &stConfigData);
		/*
        memcpy(&(m_stSettingParameter.Panel.Color), &(stConfigData.Panel.Color), sizeof(LCDPanelColor));
        wxPixelPanel::Parameter			stPixelParameter;
		memcpy(&stPixelParameter, &(m_stSettingParameter.Panel), sizeof(wxPixelPanel::Parameter));
		m_pclsPanelPreview->SetParameters(&stPixelParameter);
		*/

		UpdatePreviewPanel();
	}
	pclsDialog->Destroy();
}

void LCD_DialogSettings::OnGridEnable(void)
{
	m_stSettingParameter.Panel.EnableGrid = m_pclsCheckBoxEnableGrid->GetValue();
	m_pclsPanelPreview->SetParameter(&m_stSettingParameter.Panel);
	UpdatePreviewPanel();
}

void LCD_DialogSettings::OnPortSet(void)
{
	long		lBaudrate;
	strcmp(m_stSettingParameter.Port.PortName, m_pclsComboBox_ComPort->GetValue().c_str().AsChar());
	m_pclsComboBox_Baudrate->GetValue().ToLong(&lBaudrate);
	m_stSettingParameter.Port.Baudrate = (uint32_t)lBaudrate;
}

