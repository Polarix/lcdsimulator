#include <stdio.h>

#include "wx/wx.h"
#include "wx/intl.h"
#include "Application.h"

#include "LCD_FrameBase.h"
#include "LCD_FrameLCD.h"
#include "LCD_Common.h"


IMPLEMENT_APP(LCD_Simulator);

bool LCD_Simulator::OnInit(void)
{
	wxImage::AddHandler(new wxPNGHandler);
	wxImage::AddHandler(new wxJPEGHandler);

	wxString strConfigFilePath = LCD_DEFAULT_CONFIG_FILE;
	// Read configuration data.
	ReadConfigFile(strConfigFilePath, &g_stParameters);
	SetLanguage(wxLANGUAGE_DEFAULT);
	// Create frame;
	LCD_DeviceFrame* CFrmMain = new LCD_DeviceFrame((LCD_FrameBase*)NULL);
	//wxInitAllImageHandlers();
	//SetTopWindow(CFrmMain);
	CFrmMain->Show();
    return true;
}

void LCD_Simulator::SetLanguage(wxLanguage eLanguage)
{
	bool			bLocaleResult;
    wxLocale::AddCatalogLookupPathPrefix(".\\Lang");
    bLocaleResult = m_clsLocale.Init(eLanguage);
	if(false == bLocaleResult)
    {
        wxLogWarning(_("This language is not supported by the system."));
	}
    bLocaleResult = m_clsLocale.AddCatalog("LCDSimulator");
    if (false == bLocaleResult)
    {
        printf("Load language file failed.");
        // No language files.
    }
}

int LCD_Simulator::OnExit()
{
    return 0;
}
