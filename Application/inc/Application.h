/***************************************************************
 * Name:      LCDSimulatorMain.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2017-02-25
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include <wx/dialog.h>
#include <wx/app.h>
#include <wx/intl.h>


class LCD_Simulator : public wxApp
{
    public:
		void						SetLanguage(wxLanguage eLanguage);
	protected:
		wxLocale					m_clsLocale;  // locale we'll be using
    private:
		bool						OnInit(void);
		int							OnExit(void);

};

#endif //__INCLUDE_LCD_APPLICATION_H__
