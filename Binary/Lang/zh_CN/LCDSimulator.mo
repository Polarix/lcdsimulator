��    B      ,  Y   <      �     �     �  %   �     �     �     �  
     	             !     .     ;     Q     g     n  &   w     �     �     �  &   �  
   �     
     #  1   5     g     l     y          �     �     �     �  (   �     �     �     �                     =     \     c     i     o     {     �  
   �     �     �  	   �     �     �     �     	      	     ?	  
   [	     f	     o	     w	     �	     �	     �	     �	  
   �	  �  �	     �     �  "   �     �     �  	                  &     -     :     G     ]     s  	   �  )   �     �     �     �  !   �               2  0   ?     p     }     �     �     �  	   �     �     �  %   �     �                      !   5  '   W  	        �     �     �     �  	   �     �     �  *   �          %     2     N     m  $   �     �     �     �     �  	   �     �       '     *   :     e        ,   1   &                           (                    #                *       >      "           7   5   2   $           =   0           ;   %                           <          A   +   .       '      @      /                       B          	   3   -   ?   )      !          6          
      4   :   8   9        %s is closed. %s open failed. %s open successfully, baudrate is %u. About About LCDSimulator About. Appearance Baudrate: Cancel Clean Screen Clean screen Clean screen display. Clean screen(Ctrl+%s) Colors Columns: Command ID: %u; Data: %u; %s(0x%04X).
 Connect Device Copy Screenshots Copy Screenshots(Ctrl+%s) Copy screenshots picture to clipboard. Developers Device is not connected! Disconnect Device Disconnect device and close log window if opened. Edge Enable Grid. Error Exit Exit(Alt+F4) Exit. Grid Invalid LCD Simulator Configuration(*.xml)|*.xml LCDSimulator LCDSimulator Log LCDSimulator Settings OK Open Screenshots Folder Open Screenshots Folder(Ctrl+%s) Open screenshots saved folder. Pages: Panel Pixel Pixel size: Port settings Port: Quick Shot Quick Shot(Ctrl+%s) Quick save screen shot to file. Read Data Refresh Screen Save screen failed. Save screen to %s. Screen cleared. Screen shot cpoied to clipboard. Select a configuration file Set Column Set Page Setting Setting. Show Data Log Window Show data log Show realtime data log window. Try to connect device. Write Data Project-Id-Version: LCD Simulator
POT-Creation-Date: 2017-07-13 14:59+0800
PO-Revision-Date: 2017-07-13 14:59+0800
Last-Translator: 
Language-Team: Polarix
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.2
X-Poedit-Basepath: ../../../Frame
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _TRANS_TEXT
X-Poedit-SearchPath-0: .
 %s已关闭。 %s 打开失败. %s打开成功，波特率%ubps。 关于 关于LCDSimulator 关于。 外观设置 波特率： 取消 清除屏幕 清除屏幕 清除屏幕显示。 清除屏幕(Ctrl+%s) 配色设置 列数： 指令：%u；数据：%u；%s(0x%04X).

 连接设备 复制屏幕图像 复制屏幕图像(Ctrl+%s) 复制屏幕图像至剪贴板。 开发人员 设备未连接！ 连接设备 断开设备连接并关闭数据监视窗口。 边框颜色 显示网格。 错误 退出 退出(Alt+F4) 退出。 网格颜色 无效指令 LCD模拟器配置文件(*.xml)|*.xml LCDSimulator 通讯数据 设置 确定 打开屏幕截图目录 打开屏幕截图目录(Ctrl+%s) 打开屏幕截图文件保存目录。 页数： 面板底色 像素颜色 像素尺寸： 端口设置 端口： 屏幕截图 屏幕截图(Ctrl+%s) 快速保存当前屏幕图像至文件。 读取数据 刷新屏幕 屏幕图像保存失败。 屏幕图像已保存至 %s。 屏幕已清除。 屏幕图像已复制到剪贴板。 从外部文件导入 设定列索引 设定页索引 设置 设置。 显示数据监视窗口 数据监视 显示实时通信数据监视窗口。 尝试打开端口并连接终端设备。 写入数据 